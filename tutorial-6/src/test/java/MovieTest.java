import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MovieTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    @Test
    public void getTitle() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);

        assertEquals("Who Killed Captain Alex?", movie.getTitle());
    }

    @Test
    public void setTitle() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);

        movie.setTitle("Bad Black");

        assertEquals("Bad Black", movie.getTitle());
    }

    @Test
    public void getPriceCode() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);

        assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);

        movie.setPriceCode(Movie.CHILDREN);

        assertEquals(Movie.CHILDREN, movie.getPriceCode());
    }

    @Test
    public void equals() {
        Movie movie1 = new Movie("Kiki Delivery Service", Movie.CHILDREN);
        Movie movie2 = new Movie("The Hunger Games", Movie.REGULAR);
        Movie movie3 = new Movie("Kiki Delivery Service", Movie.CHILDREN);

        assertEquals(movie1.equals(movie2), false);
        assertEquals(movie1.equals(movie3), true);
        assertFalse(movie1.equals(null));

        assertFalse(movie1.hashCode() == movie2.hashCode());
        assertTrue(movie1.hashCode() == movie3.hashCode());
    }
}