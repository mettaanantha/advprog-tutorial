package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ThickBunBurger extends Food {
    public ThickBunBurger() {
    }

    @Override
    public double cost() {
        return 2.5;
    }

    @Override
    public String getDescription() {
        return "Thick Bun Burger";
    }
}