package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = true)
                                       String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @GetMapping("/cv")
    public String cv(@RequestParam(name="isVisitor", required = false) String isVisitor, Model model) {
        if (isVisitor == null || isVisitor.equals("")) {
            model.addAttribute("title", "This is my CV");
        } else {
            model.addAttribute("title", isVisitor + ", I hope you interested to hire me");
        }
        return "cv";
    }
}
