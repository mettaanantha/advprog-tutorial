package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class FrontendProgrammer extends Employees {
    //TODO Implement
    public FrontendProgrammer(String name, double salary){
        this.name = name;
        this.salary = salary;
        this.role = "Front End Programmer";
    }

    public double getSalary(){
        return this.salary;
    }
}