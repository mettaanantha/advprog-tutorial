package tutorial.javari;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tutorial.javari.animal.Animal;

@RestController
public class JavariController {
    // TODO Implement me!
    JavariDatabase database = new JavariDatabase();
    JsonMessage message = new JsonMessage("", "");

    public JavariController() throws IOException {
    }

    @GetMapping("/javari")
    public Object getAllAnimal() {
        List<Animal> listAnimal = database.getAnimals();
        if (listAnimal == null || listAnimal.isEmpty()) {
            return message.nothingInDatabase();
        }
        return listAnimal;
    }

    @GetMapping("/javari/{id}")
    public Object getAnimalwithId(@PathVariable Integer idAnimal) {
        Animal animal = database.getAnimalwithId(idAnimal);
        if (animal == null) {
            return message.notFoundMessage(idAnimal);
        }
        return animal;
    }

    @DeleteMapping("/javari/{id}")
    public Object deleteAnimal(@PathVariable Integer idAnimal) throws IOException {
        Animal animal = database.deleteAnimalwithId(idAnimal);
        if (animal != null) {
            Object[] messageAnimal = {message.successDeleteAnimal(), animal};
            return messageAnimal;
        }
        return message.notFoundMessage(idAnimal);
    }

    @PostMapping("/javari")
    public Object insertAnimal(@RequestBody String input) throws IOException, JSONException {
        Animal animal = database.addNewAnimal(input);
        Object[] animalMessage = {message.successAddAnimal(), animal};
        return animalMessage;
    }

}
