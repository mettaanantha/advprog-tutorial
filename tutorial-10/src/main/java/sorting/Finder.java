package sorting;

public class Finder {


    /**
     * Some searching algorithm that possibly the slowest algorithm.
     * This algorithm can search a value irregardless of whether the sequence already sorted or not.
     * @param arrOfInt is a sequence of integer.
     * @param searchedValue value that need to be searched inside the sequence.
     * @return -1 if there are no such value inside the sequence, else return searchedValue.
     */
    public static int slowSearch(int[] arrOfInt, int searchedValue) {
        int returnValue = -1;

        for (int element : arrOfInt) {
            if (element == searchedValue) {
                returnValue = element;
            }
        }

        return returnValue;
    }

    // For sorted array

    public static int binarySearch(int[] arrOfInt, int searchedValue) {
        int returnValue = -1;
        int low = 0;
        int high = arrOfInt.length + 1;

        while (low <= high) {
            int mid = (low + high) / 2;
            if (arrOfInt[mid] < searchedValue) {
                low = mid + 1;
            } else if (arrOfInt[mid] > searchedValue) {
                high = mid - 1;
            } else if (arrOfInt[mid] == searchedValue) {
                returnValue = arrOfInt[mid];
                break;
            }
        }
        return returnValue;
    }

    // For both sorted and unsorted arrays

    public static int fasterSearch(int[] arrOfInt, int searchedValue) {
        int returnValue = -1;

        for (int i = 0; i < arrOfInt.length / 2; i++) {
            if (arrOfInt[i] == searchedValue) {
                returnValue = arrOfInt[i];
                break;
            } else if (arrOfInt[arrOfInt.length - 1 - i] == searchedValue) {
                returnValue = arrOfInt[arrOfInt.length - 1 - i];
                break;
            }
        }
        return returnValue;
    }
}
