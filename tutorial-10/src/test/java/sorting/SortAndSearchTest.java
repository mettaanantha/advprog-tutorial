package sorting;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SortAndSearchTest {

    private static final int numberOfItemToBeSorted = 50000;
    private static int[] unsortedSequence;
    private static int[] sortedSequence;

    @Before
    public void setUp() {
        try {
            unsortedSequence = convertInputFileToArray(
                    "../plainTextDirectory/input/sortingProblem.txt");
            sortedSequence = convertInputFileToArray(
                    "../plainTextDirectory/input/sorted.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSearchAlgorithmSuccessfullySearchBeforeSorted() {
        int searchingResultBeforeSort2 = Finder.fasterSearch(unsortedSequence, 40738);

        Assert.assertEquals(40738, searchingResultBeforeSort2);
    }

    @Test
    public void testSortingAlgorithmSuccessfullySort() {
        int[] sortWithAlgorithm =
                Sorter.quickSort(unsortedSequence, 0, unsortedSequence.length - 1);
        Assert.assertArrayEquals(sortedSequence, sortWithAlgorithm);
    }

    @Test
    public void testSearchAlgorithmSuccessfullySearchAfterSorted() {
        int searchingResultAfterSort = Finder.binarySearch(sortedSequence, 40738);
        int searchingResultAfterSort2 = Finder.fasterSearch(sortedSequence, 40738);

        Assert.assertEquals(40738, searchingResultAfterSort);
        Assert.assertEquals(40738, searchingResultAfterSort2);
    }

    /**
     * Converting a file input into an array of integer.
     *
     * @return an array of integer that represent an integer sequence.
     * @throws IOException in the case of the file is not found because of the wrong path of file.
     */
    private static int[] convertInputFileToArray(String pathFile) throws IOException {
        File sortingProblemFile = new File(pathFile);
        FileReader fileReader = new FileReader(sortingProblemFile);
        int[] sequenceInput = new int[numberOfItemToBeSorted];

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String currentLine;
        int indexOfSequence = 0;
        while ((currentLine = bufferedReader.readLine()) != null) {
            sequenceInput[indexOfSequence] = Integer.parseInt(currentLine);
            indexOfSequence++;
        }
        return sequenceInput;
    }
}
