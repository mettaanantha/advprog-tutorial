package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {
    //TODO Implement
    public SecurityExpert(String name, double salary){
        this.name = name;
        this.salary = salary;
        this.role = "Security Expert";
    }

    public double getSalary(){
        return this.salary;
    }
}