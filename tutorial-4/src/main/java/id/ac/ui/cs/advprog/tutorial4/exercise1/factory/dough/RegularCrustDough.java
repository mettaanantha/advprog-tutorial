package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class RegularCrustDough implements Dough {
    public String toString() {
        return "Regular Crust Dough";
    }
}