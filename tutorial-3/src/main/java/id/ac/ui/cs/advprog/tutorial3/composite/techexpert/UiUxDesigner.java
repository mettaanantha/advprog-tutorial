package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {
    //TODO Implement
    public UiUxDesigner(String name, double salary){
        this.name = name;
        this.salary = salary;
        this.role = "UI/UX Designer";
    }

    public double getSalary(){
        return this.salary;
    }
}