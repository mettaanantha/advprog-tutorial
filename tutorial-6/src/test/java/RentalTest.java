import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RentalTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    private Movie movie, movie2, movie3;
    private Rental rent, rent2, rent3;

    @Before
    public void setUp() {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie2 = new Movie("Kiki Delivery Service", Movie.CHILDREN);
        movie3 = new Movie("The Avengers", Movie.NEW_RELEASE);

        rent = new Rental(movie, 3);
        rent2 = new Rental(movie2, 4);
        rent3 = new Rental(movie3, 2);
    }

    public void getMovie() {
        assertEquals(movie, rent.getMovie());
    }

    @Test
    public void getDaysRented() {
        assertEquals(3, rent.getDaysRented());
    }

    @Test
    public void moviePrice() {
        assertEquals(""+rent.moviePrice(), "3.5");
        assertEquals("3.0", ""+rent2.moviePrice());
        assertEquals("6.0", ""+rent3.moviePrice());
    }

    @Test
    public void frequentRenterPointOf() {
        assertTrue(rent.frequentRenterPointOf() == 1);
        movie.setPriceCode(Movie.NEW_RELEASE);
        assertTrue(rent.frequentRenterPointOf() == 2);
    }
}